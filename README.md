# Karabiner JSON Schema

Use it with [Codium][] (or any other editor that supports JSON schemas):

```json
{
  "$schema": "https://ale.sh/karabiner-jsonschema.json",
  "title": "Fn + F1/F2 variations",
  "rules": [
    {
      // start typing and enjoy autocomplete!
    }
  ]
}
```

[Codium]: https://vscodium.com/

When you're done, save file and run:

```shell
open "karabiner://karabiner/assets/complex_modifications/import?url=file://${PWD}/example.json"
```

(substitute `example.json` to your filename)


## Bonus

Some useful characters for your rule descriptions: ⌃⌥⇧⌘←↑→↓⌫⌦
